module.exports = {

	/**
	 * Delete the 'setupCMD' command after it is ran. Set to 'true' for the command message to be deleted
	 */
	deleteSetupCMD: true,

	initialMessage: `**Per giocare in compagnia, seleziona i giochi che vuoi giocare cliccando sulle icone qui sotto! Se ci ripensi, semplicemente clicca nuovamente l'emoji.**`,
	
	embedMessage: `
	Reagisci cliccando le emoji corrispondenti al gioco di proprio interesse.
	
	Se ci ripensi, clicca nuovamente l'emoji.	`,
	
	/**
	 * Must set this if "embed" is set to true
	 */
	embedFooter: "Ruoli",
	
	/**
	 * Set to "true" if you want all roles to be in a single embed
	 */
	embed: false,

	/**
	 * Set the embed color if the "embed" variable is et to "true"
	 * Format:
	 * 
	 * #dd9323
	 */
	embedColor: "#dd9323",

	/**
	 * Set to "true" if you want to set a thumbnail in the embed
	 */
	embedThumbnail: false,

	/**
	 * The link for the embed thumbnail if "embedThumbnail" is set to true
	 */
	embedThumbnailLink: "",
};
