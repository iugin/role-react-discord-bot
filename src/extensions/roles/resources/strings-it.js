exports.message_help = `
Ehi, sono il bot! I comandi sono:
* \`add [NOME] [EMOJI]\` per aggiungere un nuovo ruolo;
* \`del [NOME]\` per rimuovere il ruolo specificato;

PS: Non scordarti di usare il prefisso davanti ai comandi :wink:
`;
exports.message_roleAdded = `
Ben fatto! Il ruolo è stato aggiunto.
`;
exports.message_roleDeleted = `
Ruolo eliminato con successo.
`;
exports.message_error_invalidValue = `
Il valore inserito non è valido, controlla di averlo scritto correttamente!
`;
exports.message_error_insufficentPermission = `
Non hai permessi sufficienti, solo gli admin possono eseguire questo comando.
`;
exports.message_error_invalidUser = `
L'utente non è presente sul server, controlla di aver inserito il tag giusto!
`;
exports.message_react_below = `React below to get the **"%ROLE_NAME%"** role!`