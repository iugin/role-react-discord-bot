var sqlite3 = require('sqlite3').verbose();
var db = new sqlite3.Database('database/roles.db');
const strings = require("../resources/strings-it");

db.serialize(function () {
  db.run("CREATE TABLE IF NOT EXISTS roles (name TEXT PRIMARY KEY, emoji TEXT, fullname TEXT)", (res, err) => { });
});

function getRoles() {
  return new Promise((resolve, reject) => {
    db.all(`SELECT * FROM roles`, [], function (err, rows) {
      if (err) {
        reject();
      } else {
        resolve(rows);
      }
    });
  })
}

function createRole(roleName, roleEmoji, fullname) {
  return new Promise((resolve, reject) => {
    db.run('INSERT INTO roles(name, emoji, fullname) VALUES(?, ?, ?)',
      [roleName, roleEmoji, fullname], function (err, rows) {
        if (err) {
          reject();
        } else {
          resolve(rows);
        }
      });
    });
}

function removeRole(name) {
  return new Promise((resolve, reject) => {
    db.run(`DELETE FROM roles WHERE name=?`, [name], function (err, rows) {
      if (err) {
        reject();
      } else {
        resolve(rows);
      }
    });
  });
}

function getRole(name) {
  return new Promise((resolve, reject) => {
    db.get(`SELECT * FROM roles WHERE name=?`, [name], function (err, record) {
      if (err) {
        reject(err);
      } else if (record == null) {
        reject(strings.message_error_invalidValue)
      } else {
        resolve(record);
      }
    });
  });
}

exports.getRoles = getRoles;
exports.createRole = createRole;
exports.removeRole = removeRole;
exports.getRole = getRole;