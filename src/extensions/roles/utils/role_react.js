const CONFIG = require('../resources/config_role_react');
const storage = require("./storageUtils");
const strings = require("../resources/strings-it");

// Function to generate the role messages, based on your settings
function generateMessages() {
    return storage.getRoles()
        .then(roles => roles.map(r => {
            return {
                role: r.fullname,
                message: strings.message_react_below.replace("%ROLE_NAME%", r.name),
                emoji: r.emoji
            };
        }));
}

// Function to generate the embed fields, based on your settings and if you set "const embed = true;"
function generateEmbedFields() {
    return storage.getRoles()
        .then(roles => roles.map(r => {
            return {
                emoji: r.emoji,
                role: r.fullname
            };
        }));
}

// This makes the events used a bit more readable
const events = {
	MESSAGE_REACTION_ADD: 'messageReactionAdd',
	MESSAGE_REACTION_REMOVE: 'messageReactionRemove',
};

function onMessage(message) {
    if (CONFIG.deleteSetupCMD) {
        const missing = message.channel.permissionsFor(message.guild.me).missing('MANAGE_MESSAGES');
        // Here we check if the bot can actually delete messages in the channel the command is being ran in
        if (missing.includes('MANAGE_MESSAGES'))
            throw new Error("I need permission to delete your command message! Please assign the 'Manage Messages' permission to me in this channel!");
        message.delete().catch(O_o=>{});
    }

    if (!CONFIG.embed) {
        if (!CONFIG.initialMessage || (CONFIG.initialMessage === '')) 
            throw "The 'initialMessage' property is not set in the config.js file. Please do this!";

        message.channel.send(CONFIG.initialMessage);

        generateMessages().then(messages => {
            for (const { role, message: msg, emoji } of messages) {
                if (!message.guild.roles.cache.find(r => r.name === role))
                    throw `The role '${role}' does not exist!`;
    
                message.channel.send(msg).then(async m => {
                    const customCheck = message.guild.emojis.cache.find(e => e.name === emoji);
                    if (!customCheck) await m.react(emoji);
                    else await m.react(customCheck.id);
                }).catch(console.error);
            }
        })
    } else {
        if (!CONFIG.embedMessage || (CONFIG.embedMessage === ''))
            throw "The 'embedMessage' property is not set in the config.js file. Please do this!";
        if (!CONFIG.embedFooter || (CONFIG.embedMessage === ''))
            throw "The 'embedFooter' property is not set in the config.js file. Please do this!";

        const roleEmbed = new RichEmbed()
            .setDescription(CONFIG.embedMessage)
            .setFooter(CONFIG.embedFooter);

        if (CONFIG.embedColor) roleEmbed.setColor(CONFIG.embedColor);

        if (CONFIG.embedThumbnail && (CONFIG.embedThumbnailLink !== '')) 
            roleEmbed.setThumbnail(CONFIG.embedThumbnailLink);
        else if (CONFIG.embedThumbnail && message.guild.icon)
            roleEmbed.setThumbnail(message.guild.iconURL);

        const fields = generateEmbedFields();
        if (fields.length > 25) throw "That maximum roles that can be set for an embed is 25!";

        for (const { emoji, role } of fields) {
            if (!message.guild.roles.cache.find(r => r.name === role))
                throw `The role '${role}' does not exist!`;

            const customEmote = client.emojis.cache.find(e => e.name === emoji);
            
            if (!customEmote) roleEmbed.addField(emoji, role, true);
            else roleEmbed.addField(customEmote, role, true);
        }

        message.channel.send(roleEmbed).then(async m => {
            for (const r of CONFIG.reactions) {
                const emoji = r;
                const customCheck = client.emojis.cache.find(e => e.name === emoji);
                
                if (!customCheck) await m.react(emoji);
                else await m.react(customCheck.id);
            }
        });
    }
}

// This event handles adding/removing users from the role(s) they chose based on message reactions
async function onRawEvent(client, event) {
    if (!events.hasOwnProperty(event.t)) return;

    const { d: data } = event;
    const user = client.users.cache.get(data.user_id);
    const channel = client.channels.cache.get(data.channel_id);

    const message = await channel.messages.fetch(data.message_id);
    const member = message.guild.members.cache.get(user.id);

    const emojiKey = (data.emoji.id) ? `${data.emoji.name}:${data.emoji.id}` : data.emoji.name;
    let reaction = message.reactions.cache.get(emojiKey);

    if (!reaction) {
        // Create an object that can be passed through the event like normal
        const emoji = new Emoji(client.guilds.cache.get(data.guild_id), data.emoji);
        reaction = new MessageReaction(message, emoji, 1, data.user_id === client.user.id);
    }

    let embedFooterText;
    if (message.embeds[0]) embedFooterText = message.embeds[0].footer.text;

    if (
        (message.author.id === client.user.id) && (message.content !== CONFIG.initialMessage || 
        (message.embeds[0] && (embedFooterText !== CONFIG.embedFooter)))
    ) {

        if (!CONFIG.embed && (message.embeds.length < 1)) {
            const re = `\\*\\*"(.+)?(?="\\*\\*)`;
            const rolename = message.content.match(re)[1];
            storage.getRole(rolename).then(role => {
                if (member.id !== client.user.id) {
                    const guildRole = message.guild.roles.cache.find(r => r.name === role.fullname);
                    if (event.t === "MESSAGE_REACTION_ADD") member.roles.add(guildRole.id);
                    else if (event.t === "MESSAGE_REACTION_REMOVE") member.roles.remove(guildRole.id);
                }
            });
        } else if (CONFIG.embed && (message.embeds.length >= 1)) {
            const fields = message.embeds[0].fields;

            for (const { name, value } of fields) {
                storage.getRole(value).then(role => {
                    if (member.id !== client.user.id) {
                        const guildRole = message.guild.roles.cache.find(r => r.name === role.fullname);
                        if ((name === reaction.emoji.name) || (name === reaction.emoji.toString())) {
                            if (event.t === "MESSAGE_REACTION_ADD") member.roles.add(guildRole.id);
                            else if (event.t === "MESSAGE_REACTION_REMOVE") member.roles.remove(guildRole.id);
                        }
                    }
                });
            }
        }
    }
}

exports.onMessage = onMessage;
exports.onRawEvent = onRawEvent;