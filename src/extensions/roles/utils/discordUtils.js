const strings = require("../resources/strings-it");

function addRole(guild, roleName) {
  return guild.roles
    .create({
      data: {
        name: `${roleName}`,
        color: 'WHITE',
      }
    });
}

function removeRole(guild, roleName) {
  let role = guild.roles.cache.find(role => role.name === roleName)
  if (role == null) {
    return Promise.reject(strings.message_error_invalidValue)
  }
  return role.delete('The role needed to go');
}

exports.addRole = addRole;
exports.removeRole = removeRole;