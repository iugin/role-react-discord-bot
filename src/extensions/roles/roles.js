const strings = require("./resources/strings-it");
const storage = require("./utils/storageUtils");
const discordUtils = require("./utils/discordUtils");
const role_react = require("./utils/role_react");

function addRole(guild, roleName, roleEmoji) {
  return storage.getRole(roleName)
    .then(() => Promise.reject(strings.message_error_invalidValue))
    .catch(() => {
      console.log(`Adding role '${roleName}' with emoji ${roleEmoji}.`);
      // Compute role fullname
      let fullRoleName = getFullRoleName(roleName, roleEmoji)
      // Create the role on discord
      return discordUtils.addRole(guild, fullRoleName)
        .then((roleId) => console.log(`DISCORD: Created role '${fullRoleName}': ${roleId}`))
        // Save the role on storage
        .then(() => storage.createRole(roleName, roleEmoji, fullRoleName))
        .then(() => console.log(`DB: Saved the role: ${roleName} on storage`));
    })
}

function removeRole(guild, roleName) {
  // Compute role fullname
  return storage.getRole(roleName)
    .then((roleEmoji) => getFullRoleName(roleName, roleEmoji))
    .then(fullRoleName => {
      console.log(`Removing role ${fullRoleName}`);
      // Delete the role on discord
      return discordUtils.removeRole(guild, fullRoleName)
        .then(() => console.log(`DISCORD: Deleted role ${fullRoleName}`))
        // Delete role from storage
        .then(() => storage.removeRole(roleName))
        .then(() => console.log(`DB: Deleted the role: ${roleName} from storage`));
    })
}

function getFullRoleName(name, emoji) {
  return `${emoji} - ${name}`
}

function validateRoleName(str) {
  var re = /^[A-z0-9]+$/;
  return re.test(str) ? Promise.resolve(str) : Promise.reject(strings.message_error_invalidValue);
}

function validateEmoji(str) {
  var re = /^.+$/;
  return str && re.test(str) ? Promise.resolve(str) : Promise.reject(strings.message_error_invalidValue);
}

function checkPermissionToEdit(channel, member) {
  return new Promise((resolve, reject) => {
    // TODO check if management team
    if(member.roles.cache.find(r => r.name === "admin")){
      resolve();
    } else {
      reject(strings.message_error_insufficentPermission);
    }
  });
}

function logError(member, error, message) {
  console.log(`ERROR: ${member} caused ${error}`);
  console.log(message);
  message.reply(message);
}

function onMessage(command, args, message) {
  const member = message.member;
  const member_name = member.user;
  const guild = message.guild;
  const channel = message.channel;

  // Make sure the command can only be ran in a server
  if (!guild) return;

  // The bot must have the permission to manage roles
  if(!guild.me.hasPermission("MANAGE_ROLES"))

  // We don't want the bot to do anything further if it can't send messages in the channel
  if (guild && !channel.permissionsFor(guild.me).missing('SEND_MESSAGES')) return;

  const missing = channel.permissionsFor(guild.me).missing('MANAGE_MESSAGES');
  // Here we check if the bot can actually add recations in the channel the command is being ran in
  if (missing.includes('ADD_REACTIONS'))
      throw new Error("I need permission to add reactions to these messages! Please assign the 'Add Reactions' permission to me in this channel!");

  if (command === "role") {
    command = args.shift().toLowerCase();

    switch (command) {
      case "help":
        message.reply(strings.message_help);
        break;
      case "add":
        checkPermissionToEdit(channel, member)
          .then(() => validateEmoji(args[1]))
          .then(() => validateRoleName(args[0]))
          .then(role => addRole(guild, role, args[1]))
          .then(() => message.reply(strings.message_roleAdded))
          .catch((err) => logError(member_name, err, message))
        break;
      case "del":
        checkPermissionToEdit(channel, member)
          .then(() => validateRoleName(args[0]))
          .then((role) => removeRole(guild, role))
          .then((m) => message.reply(strings.message_roleDeleted))
          .catch((err) => logError(member_name, err, message))
        break;
      case "message":
        role_react.onMessage(message)
        break;
    }
  }
}

exports.init = function init(client) {
  client.on('raw', (e) => role_react.onRawEvent(client, e));
  return onMessage;
}