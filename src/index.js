// Load up the discord.js library
const Discord = require("discord.js");

const extensions = require("./utils/extensionsUtils");

// This is your client. Some people call it `bot`, some people call it `self`, 
// some might call it `cootchie`. Either way, when you see `client.something`, or `bot.something`,
// this is what we're refering to. Your client.
const client = new Discord.Client();
extensions.setClient(client);

// Here we load the config.json file that contains our token and our prefix values. 
const config = require("../config/config.json");
// config.token contains the bot's token
// config.prefix contains the message prefix.

client.on("ready", () => {
  // This event will run if the bot starts, and logs in, successfully.
  console.log(`Bot has started, with ${client.users.size} users, in ${client.channels.size} channels of ${client.guilds.size} guilds.`); 
  // Example of changing the bot's playing game to something useful. `client.user` is what the
  // docs refer to as the "ClientUser".
  // client.user.setActivity(`Serving ${client.guilds.size} servers`);
  client.user.setActivity('Type ' + config.prefix + 'help for a list of commands')
});

client.on("message", async message => {
  // This event will run on every single message received, from any channel or DM.
  
  // It's good practice to ignore other bots. This also makes your bot ignore itself
  // and not get into a spam loop (we call that "botception").
  if(message.author.bot) return;
  
  // Also good practice to ignore any message that does not start with our prefix, 
  // which is set in the configuration file.
  if(message.content.indexOf(config.prefix) !== 0) return;
  
  // Here we separate our "command" name, and our "arguments" for the command. 
  // e.g. if we have the message "+say Is this the real life?" , we'll get the following:
  // command = say
  // args = ["Is", "this", "the", "real", "life?"]
  const args = message.content.slice(config.prefix.length).trim().split(/ +/g);
  const command = args.shift().toLowerCase();
  
  switch (command) {

    default:
      extensions.onMessage(command, args, message);
      break;
  }
});

extensions.add('roles');

client.login(config.token);

process.on('unhandledRejection', err => {
  const msg = err && err.stack ? err.stack.replace(new RegExp(`${__dirname}/`, 'g'), './') : err;
console.error("Unhandled Rejection", msg);
});
