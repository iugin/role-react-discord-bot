FROM node:12-slim

WORKDIR /app

# Install the dependencies
RUN set -e -x && \
    apt-get update && apt-get install --no-install-recommends -y git ca-certificates && \
    apt-get purge -y --auto-remove && \
    rm -rf \
        /tmp/* \
        /var/tmp/* \
        /var/lib/apt/lists/*

# Install pm2
RUN npm install pm2 -g
RUN pm2 install pm2-auto-pull
# RUN pm2 install pm2-server-monit

# Pull the project
RUN git clone https://gitlab.com/iugin/role-react-discord-bot.git .

# Install app dependencies
ENV NPM_CONFIG_LOGLEVEL warn
RUN npm install --production

# Show current folder structure in logs
# RUN ls -al -R

VOLUME ["/app/database", "/app/config"]

CMD [ "pm2-runtime", "start", "pm2.json" ]